## Specification for writing an Interpreter Service

This document outlines the specifications for developing an interpreter Service.
The purpose of the interpreter is to read tests results files and publish these results in a readable format for other services.

The interpreter service provides two endpoints: 
- one for analysing and transmitting the results of one or multiple targeted tests
- the other for publishing a notification that contains the results of each test found in the received test result files

### I. Result interpretation of targeted tests

The interpreter subscribes to events of type `squashtest.org/v1alpha1/ReportInterpreterInput`, which conform to the schema:

```json
{
  "$schema": "http://json-schema.org/draft/2019-09/schema#",
  "title": "JSON SCHEMA for squashtest.org/v1alpha1 ReportInterpreterInput manifests",
  "type": "object",
  "properties": {
    "apiVersion": {"type": "string", "pattern": "^squashtest.org/v1alpha1$"},
    "kind": {"type": "string", "pattern": "^ReportInterpreterInput$"},
    "metadata": {
      "type": "object",
      "properties": {
        "name": {"type": "string"},
        "workflow_id": {"type": "string"},
        "job_id": {"type": "string"},
        "job_origin": {"type": "array", "items": {"type": "string"}},
        "step_id": {"type": "string"},
        "labels": {
          "type": "object",
          "patternProperties": {
            "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {"type": "string"}
          },
          "minProperties": 1,
          "additionalProperties": false
        }
      },
      "additionalProperties": true,
      "required": ["name", "workflow_id", "job_id", "job_origin", "step_id"]
    },
    "with": {
      "properties":{
        "testTechnology": {
          "type": "string"
        },
        "testDefinition": {
          "type": "string"
        },
        "attachments":{
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      },
      "additionalProperties": false
    }
  },
  "required": ["apiVersion", "kind", "metadata"],
  "additionalProperties": false
}
```

ReportInterpreterInput example:

```json
{
  "apiVersion": "squashtest.org/v1alpha1",
  "kind": "ReportInterpreterInput",
  "metadata": {
    "workflow_id": "64d5cbad-a0d1-4262-9ab7-b5dccc642acd",
    "name": "squashTMJob-0",
    "job_origin": [],
    "job_id": "ebf3938a-6982-4971-abfb-f6763a0e6f09",
    "step_id": "a9db2857-d72f-45af-899c-1e9af7dea9ae"
  },
  "with": {
    "attachments": [
      "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_html-report.tar",
      "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_report.xml"
    ],
    "testTechnology": "cucumber5/execute@v1",
    "testDefinition": "new-cucumber-stock-management/src/test/resources/DuplicateTags.feature#Stock Management To Check Duplicate Execution#Current stock duplicate"
  }
}
```

Implementation should locate the targeted execution status using test definition provided in `with.testDefinition` attribute and browsing through the result files whose urls are present in the `with.attachments` attribute.

Once the status is computed, the interpreter publishes a `squashtest.org/v1alpha1/ReportInterpreterOutput` event which conforms to the schema:

```json
{
  "$schema": "http://json-schema.org/draft/2019-09/schema#",
  "title": "JSON SCHEMA for squashtest.org/v1alpha1 ReportInterpreterOutput manifests",
  "type": "object",
  "properties": {
    "apiVersion": {
      "type": "string",
      "pattern": "^squashtest.org/v1alpha1$"
    },
    "kind": {
      "type": "string",
      "pattern": "^ReportInterpreterOutput$"
    },
    "metadata": {
      "type": "object",
      "properties": {
        "name": {"type": "string"},
        "workflow_id": {"type": "string"},
        "job_id": {"type": "string"},
        "job_origin": {"type": "array", "items": {"type": "string"}},
        "labels": {
          "type": "object",
          "patternProperties": {
            "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {"type": "string"}
          },
          "minProperties": 1,
          "additionalProperties": false
        }
      },
      "additionalProperties": true,
      "required": ["name", "workflow_id", "job_id", "job_origin"]
    },
    "with": {
      "type": "object",
      "properties": {
        "reportStatus": {
          "type": "string",
          "enum": ["PASS", "FAIL", "ERROR"]
        }
      },
      "additionalProperties": false
    }
  },
  "required": ["apiVersion", "kind", "metadata"],
  "additionalProperties": false
}
```

ReportInterpreterOutput example:

```json
{
  "apiVersion": "squashtest.org/v1alpha1",
  "kind": "ReportInterpreterOutput",
  "metadata": {
    "workflow_id": "64d5cbad-a0d1-4262-9ab7-b5dccc642acd",
    "name": "squashTMJob-0",
    "job_origin": [],
    "job_id": "ebf3938a-6982-4971-abfb-f6763a0e6f09",
    "step_id": "a9db2857-d72f-45af-899c-1e9af7dea9ae"
  },
  "with": {
    "reportStatus": "PASS"
  }
}
```
The message is then forwarded to the Publisher service which will publish the status of the targeted tests by calling an endpoint of the TM Publisher Plugin. This plugin handles the modification of test statuses in SquashTm.

#### Sequence Diagram
```mermaid
sequenceDiagram
   participant Interpreter
   participant EventBus
   participant Publisher Service
   participant TM Publisher Plugin
   EventBus->>Interpreter: ReportInterpreterInput
   Interpreter->>EventBus: ReportInterpreterOutput
   EventBus->>Publisher Service: ReportInterpreterOutput
   Publisher Service->>TM Publisher Plugin: AutomatedExecutionState
```
### II. Results Notification

The interpreter subscribes to events of type `opentestfactory.org/v1alpha1/ExecutionResult` which conform to the schema:

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1alpha1 ExecutionResult manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "const": "opentestfactory.org/v1alpha1"
        },
        "kind": {
            "const": "ExecutionResult"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "workflow_id": {
                    "type": "string"
                },
                "job_id": {
                    "type": "string"
                },
                "job_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_id": {
                    "type": "string"
                },
                "step_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_origin_status": {
                    "type": "object",
                    "patternProperties": {
                        "^[a-zA-Z0-9-]+$": {
                            "type": "number"
                        }
                    }
                },
                "step_sequence_id": {
                    "type": "number"
                },
                "labels": {
                    "type": "object",
                    "patternProperties": {
                        "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                            "type": "string"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                },
                "attachments": {
                    "type": "object",
                    "minProperties": 1
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id",
                "job_id",
                "step_id",
                "step_sequence_id"
            ]
        },
        "attachments": {
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 1
        },
        "outputs": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z_][a-zA-Z0-9_-]+$": {
                    "type": "string"
                }
            },
            "minProperties": 1,
            "additionalProperties": false
        },
        "logs": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "status": {
            "type": "number"
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata"
    ],
    "additionalProperties": false
}
```

ExecutionResult example:

```json
{
  "apiVersion": "opentestfactory.org/v1alpha1",
  "kind": "ExecutionResult",
  "metadata": {
    "name": "run11",
    "namespace": "default",
    "workflow_id": "64d5cbad-a0d1-4262-9ab7-b5dccc642acd",
    "job_id": "ebf3938a-6982-4971-abfb-f6763a0e6f09",
    "job_origin": [],
    "step_id": "af812cf6-cb84-4942-bb57-ec93093642db",
    "step_origin": [
      "a9db2857-d72f-45af-899c-1e9af7dea9ae"
    ],
    "step_sequence_id": 9,
    "channel_id": "da8454c8-b2d8-4773-ac11-8047e85e8c9f",
    "creationTimestamp": "2023-05-17T15:58:35.624081",
    "channel_os": "linux",
    "attachments": {
      "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_html-report.tar": {
        "uuid": "d7f9cfb4-0f9a-44c1-9392-f2e8191bb829"
      },
      "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_report.xml": {
        "uuid": "a0f6c5e6-2863-4818-99bd-ec11467d293d",
        "type": "application/vnd.opentestfactory.cucumber-surefire+xml"
      }
    }
  },
  "status": 2,
  "attachments": [
    "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_html-report.tar",
    "/tmp/ebf3938a-6982-4971-abfb-f6763a0e6f09_9_report.xml"
  ]
}
```
Implementation should analyse the received result files and interpret the result of each test within the files. Then, the service should publish a `opentestfactory.org/v1alpha1/Notification` event containing the results. It conforms to the schema:

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1alpha1 Notification manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "const": "opentestfactory.org/v1alpha1"
        },
        "kind": {
            "const": "Notification"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "namespace": {
                    "type": "string"
                },
                "workflow_id": {
                    "type": "string"
                },
                "attachment_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "job_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id"
            ]
        },
        "spec": {
            "type": "object",
            "properties": {
                "worker": {
                    "type": "object",
                    "properties": {
                        "worker_id": {
                            "type": "string"
                        },
                        "status": {
                            "type": "string",
                            "enum": [
                                "setup",
                                "teardown"
                            ]
                        }
                    },
                    "additionalProperties": false,
                    "required": [
                        "worker_id",
                        "status"
                    ]
                },
                "testResults": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {
                                "type": "string"
                            },
                            "name": {
                                "type": "string"
                            },
                            "status": {
                                "type": "string",
                                "enum": [
                                    "SUCCESS",
                                    "FAILURE",
                                    "ERROR"
                                ]
                            }
                        }
                    }
                },
                "logs": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "outcome": {
                    "type": "string"
                },
                "conclusion": {
                    "type": "string"
                }
            },
            "patternProperties": {
                "^.*\\..*$": {
                    "type": [
                        "string",
                        "array",
                        "object"
                    ]
                }
            },
            "additionalProperties": false
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata",
        "spec"
    ],
    "additionalProperties": false
}
```
Notification example:

```json
{
  "apiVersion": "opentestfactory.org/v1alpha1",
  "kind": "Notification",
  "metadata": {
    "name": "run11",
    "workflow_id": "3247bf5d-2501-4dcc-a217-3a2900f07245",
    "attachment_origin": [
      "a0f6c5e6-2863-4818-99bd-ec11467d293d"
    ]
  },
  "spec": {
    "testResults": [
      {
        "attachment_origin": "a0f6c5e6-2863-4818-99bd-ec11467d293d",
        "id": "b0f4947e-4788-4f2f-a840-af7f79b69c90",
        "name": "test_gherkin#Aucun ajout",
        "status": "SUCCESS"
      },
      {
        "attachment_origin": "a0f6c5e6-2863-4818-99bd-ec11467d293d",
        "id": "7be5501a-480a-4621-866a-60dab0db2f60",
        "name": "test_gherkin#Ajout de stock 1",
        "status": "ERROR"
      },
      {
        "attachment_origin": "a0f6c5e6-2863-4818-99bd-ec11467d293d",
        "id": "fe03997d-7d3d-42f8-a676-84228a2c3283",
        "name": "test_gherkin#Ajout de stock 2",
        "status": "FAILURE"
      }
    ]
  }
}
```
This notification will be consumed by relevant services, particularly the QualityGate service.
#### Sequence diagram
```mermaid
sequenceDiagram
   participant Interpreter
   participant EventBus
   participant QualityGate
   EventBus->>Interpreter: ExecutionResult
   Interpreter->>EventBus: Notification
   EventBus->>QualityGate: Notification
```
